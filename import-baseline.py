import pymongo
from netCDF4 import Dataset
from FileInfo import NCFileInfo
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, shiftgrid
from scipy import stats
client = pymongo.MongoClient("mongodb://localhost:27017/")

# create DB if not exist and connect to it
dbname = 'climatedb_2019'
db = client[dbname]


def calculate_baseline(dataset, baseline_start=1961, baseline_end=1991):
    """
        return as np array with null data = -- (-99.9)
    """
    ds_startyear = int(str(dataset['time'][:][0])[:4])
    start_ind = baseline_start - ds_startyear
    end_ind = baseline_end - ds_startyear
    months = list(dataset.variables.keys())[3:]
    # months can be Jan..Dec + Ann or Ann only
    if len(months) > 1:
        # discard Ann
        months = months[:-1]
    else:
        months = [months[0]]
    baseline_data = 0
    for m in months:
        baseline_data += np.nanmean(ds[m][start_ind:end_ind][:], axis=0)
    baseline_data = baseline_data / (len(months))

    return baseline_data


def calculate_annual_baseline(dataset, baseline_start=1961, baseline_end=1991):
    ds_startyear = int(str(dataset['time'][:][0])[:4])
    start_ind = baseline_start - ds_startyear
    end_ind = baseline_end - ds_startyear
    baseline = ds['Ann'][start_ind:end_ind]
    annual_baseline = np.array(np.nanmean(baseline, axis=(1, 2)))

    return annual_baseline


def calculate_descriptive_stat(baseline_data, confidence_int=0.9):
    global_mean = np.round(np.nanmean(baseline_data), 3)
    global_var = np.round(np.nanvar(baseline_data), 3)
    global_max = np.round(np.amax(baseline_data), 3)
    global_min = np.round(np.amin(baseline_data), 3)
    confidence_interval_range = np.round(
        stats.norm.interval(
            confidence_int, global_mean,
            np.nanstd(baseline_data) /
            np.count_nonzero(~np.isnan(global_mean))), 3).tolist()

    return {
        'global_mean': round(float(global_mean), 3),
        'global_variance': round(float(global_var), 3),
        'global_max': round(float(global_max), 3),
        'global_min': round(float(global_min), 3),
        'confidence_interval': confidence_interval_range
    }


if __name__ == '__main__':
    datasets = ["ghcndex", "hadex2"]
    for dataset in datasets:
        directory = "./{}_{}/".format(dataset, 'current')
        for file_name in os.listdir(directory):
            ds = Dataset(directory + file_name)
            file_name_split = file_name.lower().split('_')
            collection_name = f"{dataset}_{file_name_split[1]}_baseline"
            collection = db[collection_name]
            collection.drop()
            baseline = calculate_baseline(ds, 1961, 1991)
            annual_baseline = calculate_annual_baseline(ds, 1961, 1991)
            dictionary_data = {}
            dictionary_data['start_year'] = 1961
            dictionary_data['end_year'] = 1990

            # shift baseline
            lon_half_index = int(baseline.shape[1] / 2)
            shifted_baseline = np.roll(baseline, lon_half_index,
                                       axis=1).tolist()
            dictionary_data['data'] = shifted_baseline
            dictionary_data['annual_baseline'] = np.round(
                annual_baseline.tolist(), 3).tolist()

            dictionary_data.update(calculate_descriptive_stat(baseline))
            collection.insert(dictionary_data)
            print(collection_name)