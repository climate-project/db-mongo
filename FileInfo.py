# Get index info from nc file
from netCDF4 import Dataset
import numpy as np
import os


class NCFileInfo:
    def __init__(self, directory, file_name):
        self.file_name = file_name
        self.ds = Dataset(directory + file_name, mode="r", format="NetCDF4")
        self.dataset_name = self.get_dataset_name()
        self.index = self.get_index()
        self.year = self.get_year()
        self.grid = self.get_grid()
        self.mask = self.get_mask()
        self.time_var = self.get_time_var()
        self.lat = self.ds['lat'][:].tolist()
        self.lon = np.roll(self.ds['lon'][:], int(len(self.ds['lon'][:])/2))
        self.lon[:int(len(self.lon)/2)] = self.lon[:int(len(self.lon)/2)] - 360
        self.lon = self.lon.tolist()
        self.coordinate = self.get_coordinate()
        self.geojson = self.get_geojson()

    def get_dataset_name(self):
        return self.file_name[:-3].split("_")[0]

    def get_index(self):
        return self.file_name[:-3].split("_")[1]

    def get_year(self):
        years = self.file_name[:-3].split("_")[2].split("-")
        start = int(years[0])
        end = int(years[1])
        return list(range(start, end+1, 1))

    def get_grid(self):
        grids = self.file_name[:-3].split("_")[5][:-3].split("x")
        return {"lon_step": float(grids[0]), "lat_step": float(grids[1])}

    def get_mask(self):
        return self.file_name[:-3].split("_")[-1]

    def get_time_var(self):
        # Availavle time variable
        variables = list(self.ds.variables.keys())[3:]
        return variables

    def get_coordinate(self):
        lon = np.array(self.lon)
        lat = np.array(self.lat)
        # create (x, y) = (lon, lat) cartesian product
        lon_lat = np.transpose(
            [np.tile(lon, len(lat)), np.repeat(lat, len(lon))]
        )
        return lon_lat.tolist()

    def get_geojson(self):
        coordinate_feature = []
        for i in self.coordinate:
            coordinate_feature.append({
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": i
                }
            })
        geojson_obj = {
            'type': 'FeatureCollection',
            'features': coordinate_feature
        }
        return geojson_obj


if __name__ == "__main__":
    directory = "./ghcndex_current/"
    for file_name in os.listdir(directory):
        f = NCFileInfo(directory, file_name)
        data = {
            "index": f.index,
            "year": f.year,
            "mask": f.mask,
            "time_var": f.time_var
        }
        print(data)
