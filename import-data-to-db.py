import pymongo
from netCDF4 import Dataset
from FileInfo import NCFileInfo
import numpy as np
import pandas as pd
import os

client = pymongo.MongoClient("mongodb://localhost:27017/")

# create DB if not exist and connect to it
dbname = 'climatedb_2019'
db = client[dbname]
branch = 'current'

datasets = ["ghcndex", "hadex2"]
for ds in datasets:
    directory = "./{}_{}/".format(ds, branch)
    folder_name = ds
    for file_name in os.listdir(directory):
        f = NCFileInfo(directory, file_name)
        collection_name = f"{folder_name}_{f.index}".lower()
        collection = db[collection_name]
        collection.drop()
        ds = Dataset(directory + file_name, 'r')
        for year in range(ds['time'].shape[0]):
            year_data = []
            for month in f.time_var:
                # cast to np.darray
                # Use pandas to change -99.9 to None
                # I dont know why project 2018 use try-except but it work!!!
                try:
                    df = pd.DataFrame(ds[month][year][:])
                    df = df.where(pd.notnull(df), None)
                    # shift data from lon = 0..360 to -180 to 180
                    # result is np array
                    shifted_df = np.roll(df, int(df.shape[1] / 2), axis=1)
                    data = shifted_df.tolist()
                    year_data.append(data)
                except:
                    break
            dict_data = {"year": f.year[year], "data": year_data}
            collection.insert(dict_data)
            print(f"created: {f.dataset_name} {f.index} {f.year[year]}")
