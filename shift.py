import numpy as np

def cartesian(x, y):
    return np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])

def shift_lon(lons):
    lons = np.roll(lons, int(len(lons)/2))
    lons[:int(len(lons)/2)] -= 360
    return lons

def shift_data(data):
    data = np.roll(data, data.shape[1]//2, axis=1)
    return data