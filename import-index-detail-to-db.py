import pymongo
from netCDF4 import Dataset
import numpy as np
import pandas as pd

client = pymongo.MongoClient("mongodb://localhost:27017/")

# create DB if not exist and connect to it
dbname = 'climatedb_2019'
db = client[dbname]
# create collection
collection = db["index_detail"]
# drop(delete) collection
collection.drop()

df = pd.read_csv('index_definitions.csv')
indices = df['index']
meanings = df['short']
definitions = df['definition']
units = df['unit']
types = df['type']
measurements = df['measurement']
color_map = df['color_map']

for i in range(len(indices)):
    index_meaning = {
        "index": indices[i],
        "short_definition": meanings[i],
        "definition": definitions[i],
        "unit": units[i],
        "type": types[i],
        "measurement": measurements[i],
        "color_map": color_map[i]
    }
    collection.insert_one(index_meaning)
    print(f"create: {indices[i]}")

# create index from 'index' field
collection.create_index("index")