import pymongo
from FileInfo import NCFileInfo
import os
import datetime
from netCDF4 import Dataset
import numpy as np
import shift
import cartesian_product
import geojson_gen


client = pymongo.MongoClient("mongodb://localhost:27017/")
# create DB if not exist and connect to it
dbname = "climatedb_2019"
db = client[dbname]
# create collection
collection = db["dataset"]
# drop(delete) collection
collection.drop()


# NC to Mongo
datasets = ["ghcndex", "hadex2"]
for ds in datasets:
    directory = "./{}_current/".format(ds)
    indices = []
    for file_name in os.listdir(directory):
        f = NCFileInfo(directory, file_name)
        if f.mask == "LSmask":
            index_info = {"index": f.index, "month": f.time_var}
            indices.append(index_info)
            print(f"import: {f.dataset_name}-{f.index}")
    dataset = {
        "source": "mongodb",
        "dataset": ds,
        "gridcenter": f.coordinate,
        "year": f.year,
        "lat": f.lat,
        "lon": f.lon,
        "gridsize": f.grid,
        "indices": indices,
        "geojson_gridcenter": f.geojson,
    }
    print(f"created: {ds}")
    collection.insert_one(dataset)


# NC only

dataset_name = "MPI-ESM-MR"
source_path = f"./{dataset_name}/"
dataset_types = os.listdir(source_path)

# get all dataset_type (rcp type) that available in this Dataset
data_type_dict = []
for dataset_type in dataset_types:
    path = f"{source_path}/{dataset_type}/"
    files = os.listdir(path)

    # first and last file in type for calculate date range
    fist_ds = Dataset(f"{path}/{files[0]}")
    last_ds = Dataset(f"{path}/{files[-1]}")

    first_date_offset = datetime.datetime.strptime(
        fist_ds["time"].units.split(" ")[2], "%Y-%m-%d"
    )
    last_date_offset = datetime.datetime.strptime(
        last_ds["time"].units.split(" ")[2], "%Y-%m-%d"
    )

    start_date = first_date_offset + datetime.timedelta(
        np.asscalar(fist_ds["time"][0].data)
    )
    end_date = last_date_offset + datetime.timedelta(
        np.asscalar(last_ds["time"][-1].data)
    )

    indices = list(fist_ds.variables.keys())[-4:]
    # type details
    type_dict = {
        "type": dataset_type,
        "start_date": start_date.strftime("%Y-%m-%d"),
        "end_date": end_date.strftime("%Y-%m-%d"),
        "indices": indices,
    }

    data_type_dict.append(type_dict)

# import grid coordinate
path = f"{source_path}/{dataset_types[0]}/"
files = os.listdir(path)
ds = Dataset(f"{path}/{files[0]}")

lats = np.round(np.array(ds["lat"][:]), 4)
lons = np.round(shift.shift_lon(np.array(ds["lon"][:])), 4)
grid_center = cartesian_product.orderpair(lons, lats)

# grid polygon geojson for frontend openlayers
lon_bounds = np.array(ds["lon_bnds"][:])
lat_bounds = np.array(ds["lat_bnds"][:])
lon_bounds = np.where(lon_bounds >= 180, lon_bounds - 360, lon_bounds)
lon_bounds = np.roll(lon_bounds, lon_bounds.shape[0] // 2, axis=0)
geojson_polygons = []
for lon_bound in lon_bounds:
    for lat_bound in lat_bounds:
        poly = cartesian_product.orderpair(lon_bound, lat_bound)
        geojson_polygons.append(poly)
grid_polygon = geojson_gen.gen_geojson_polygons(geojson_polygons)

gridcenter_features = []
for i in grid_center:
    gridcenter_features.append(
        {"type": "Feature", "geometry": {"type": "Point", "coordinates": i.tolist()}}
    )
geojson_gridcenter = {"type": "FeatureCollection", "features": gridcenter_features}

dataset = {
    "source": "nc",
    "dataset": dataset_name,
    "types": data_type_dict,
    "lat": lats.tolist(),
    "lon": lons.tolist(),
    "gridcenter": grid_center.tolist(),
    "geojson_gridpolygon": grid_polygon,
    "geojson_gridcenter": geojson_gridcenter,
}
collection.insert_one(dataset)
print(f"created: {dataset['dataset']}")
