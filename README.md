# db-mongo

- import climate data from `*.nc` files to mongo db
- import climate index-meaning from web

## Download `.nc` file

[Climdex](https://www.climdex.org/view_download.html)

- GHCNDEX
- HadEX2

## Download data

[NOAA](https://www.climate.gov/maps-data/datasets)

> Past Weather by Zip Code > [Data Access](https://www.ncdc.noaa.gov/cdo-web/search)

## Index definition

[Climdex Indices](https://www.climdex.org/learn/indices/)

for more details

[Paper GHCNDEX 2013](https://www.climdex.org/publications/Donat_etal2013_GHCNDEX_bams-d-12-00109.1.pdf)

[Paper HADEX2 2013](https://www.climdex.org/publications/Donat_etal2013_HadEX2_JGR_10.1002_jgrd.50150.pdf)

## file name details

```shell
GHCND_CSDI_1951-2019_RegularGrid_global_2.5x2.5deg_LSmask
x: lon_step = 2.5 (first) 0..360 -> ในการพล็อต shift เป็น -180..180
y: lat_step = 2.5 (second) -90..90
```

## SQL vs NoSQL

| SQL         | NoSQL                  |
| ----------- | ---------------------- |
| database    | database               |
| table       | collection             |
| rows/record | document/record (BSON) |
| column      | field                  |

## Dump & Restore Database

```shell
mongodump --host <hostname:port> --db <database> --username <username> --password <password> --out <path>
mongorestore --db <new_db name> <dump path>
```

## import-baseline.py

- คำนวณ baseline ปี 1961-1990 บันทึกเป็น collection `dataset_index_baseline`

```
ข้อมูลที่เก็บ
start/end year
data: [#spatial baseline]
global_annual_baseline: [1961, .... 1990]
descriptive_stats
```

## import-data-to-db.py

- นำข้อมูลจากไฟล์ nc ไปลงใน database (ต้องมีการ shift data ก่อน เนื่องจาก lon ที่ต้องการคือ -180 180) บันทึกใน collection `dataset_index`
- ปัญหาที่พบคือไม่สามารถมี document size ที่ใหญ่เกินจึงต้องออกแบบแบบนี้, อาจจะใช้จาก nc ได้ ซึ่งน่าจะสะดวกกว่า, เก็บแค่ baseline ใน DBs

```
data จะเป็นอาเรย์สองมิติ
row (lat) = เรียงตั้งแต่ -90 .. 90
col (lon) = เรียงตั้งแต่ -180 .. 180
```

## import-dataset-info.py

- import ข้อมูลเกี่ยวกับ dataset สำหรับใช้เป็น select บันทึกใน collection `dataset`
- เก็บเกี่ยวกับ dataset มี index ไรบ้าง, เริ่มต้นปีไหน จบปีไหน, แต่ละ index มีเดือนอะไรบ้าง

## import-index-details-to-db.py

- เก็บข้อมูลความหมายของ index และ colormap ที่ใช้ บันทึกใน collection `index_detail`
