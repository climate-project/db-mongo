# Use Beautifulsoup to get index and its meaning from climdex website
# and export to CSV file
import requests as req
from bs4 import BeautifulSoup
import pandas as pd

web_url="https://www.climdex.org/learn/indices/"
web = req.get(web_url)
soup = BeautifulSoup(web.content, "html.parser")
indices = soup.find_all("h5", {"class": "card-header"})
meanings = soup.find_all("h5", {"class": "card-title"})
indices_text = [index.text for index in indices]
meanings_text = [meaning.text for meaning in meanings]

csv_file = "bs4_index_meaning.csv"

# pandas dataframe
df = pd.DataFrame({
    'index': indices_text,
    'meaning': meanings_text
    })
# export dataframe to csv
df.to_csv(csv_file, index=None, header=True)